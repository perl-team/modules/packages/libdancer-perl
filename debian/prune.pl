#!/usr/bin/perl

use warnings;
use strict;

s/(?<="jquery.min.js" => )FILE/\\\&link_jquery/;

print <<'EOT' if /^sub jquery_minified/;
sub link_jquery {
    my $path = shift;

    my $target = '/usr/share/javascript/jquery/jquery.min.js';

    if ( -e $target ) {
        symlink( $target, $path ) or die "symlink($target, $path): $!\n";
    }
    else {
        # fallback to the embedded copy
        jquery_minified($path);
    }
}

EOT

print $_
    if not /^sub write_bg/ .. /^\}/
        and not /^sub write_logo/ .. /\}/
        and not /^sub write_favicon/ .. /\}/
        and not /write_bg/
        and not /write_logo/
        and not /write_favicon/
        and not /code.jquery.com/
        and not /Grab jQuery/;
